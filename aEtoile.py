from noeud import *
from math import sqrt

class aEtoile:
    """
        Cette classe implemente l'algorithme a*.
        ...
        Attributs
        ----------
        initial : Noeud
            Represente le noeud initial
        objectif : Noeud
            Represente le noeud objectif
        fonctionCout : int
            Represente la fonction cout a utiliser
        fonctionHeuristique : int
            Represente la fonction heuristique a utiliser
    """

    def __init__(self, initial, objectif,fonctionCout = 1,fonctionHeuristique =1):
        self.initial = initial
        self.fonctionCout = fonctionCout
        self.fonctionHeuristique = fonctionHeuristique
        initial.setName(initial.ville.distance(objectif.ville))
        self.objectif = objectif

    def fonctionCout1(self,noeud : Noeud,villeVoisine):
        """
            Cette fonction permet de calculer la valeur de la fonction cout
            Elle correspond a la somme des couts des villes parcourues

            Parametres
            ----------
            noeud : Noeud
                Le noeud courant
            villeVoisine : Ville
                La ville voisine du noeud courant
        """
        return villeVoisine[1]+noeud.g
    
    def fonctionCout2(self,noeud : Noeud,villeVoisine):
        """
            Cette fonction permet de calculer la valeur de la fonction cout
            Elle correspond à la hauteur du noeud dans l'arbre

            Parametres
            ----------
            noeud : Noeud
                Le noeud courant
            villeVoisine : Ville
                La ville voisine du noeud courant
        """
        return 1+noeud.g
    
    def fonctionHeuristique1(self,villeVoisine):
        """
            Cette fonction permet de calculer la valeur de la fonction heuristique
            Elle correspond a la distance entre la ville du noeud courant et la ville objectif en utilisant la formule de Haversine

            Parametres
            ----------
            villeVoisine : Ville
                La ville voisine du noeud courant
        """
        return villeVoisine[0].distance(self.objectif.ville)
    
    def fonctionHeuristique2(self,noeud: Noeud,villeVoisine):
        """
            Cette fonction permet de calculer la valeur de la fonction heuristique
            Elle correspond a la distance entre la ville du noeud courant et la ville objectif en utilisant la distance de Manhattan

            Parametres
            ----------
            noeud : Noeud
                Le noeud courant
            villeVoisine : Ville
                La ville voisine du noeud courant
        """
        lat1 = math.radians(noeud.ville.latitude)
        lat2 = math.radians(villeVoisine[0].latitude)
        lon1 = math.radians(noeud.ville.longitude)
        lon2 = math.radians(villeVoisine[0].longitude)
        dlon = abs(lon2 - lon1)
        dlat = abs(lat2 - lat1)  
        return dlat + dlon
    
        
    def fonctionHeuristiqueChoisi(self,noeud: Noeud,villeVoisine):
        """
            Cette fonction permet de choisir la fonction heuristique a utiliser 

            Parametres
            ----------
            noeud : Noeud
                Le noeud courant
            villeVoisine : Ville
                La ville voisine du noeud courant
        """
        if(self.fonctionHeuristique == 1):
            return self.fonctionHeuristique1(villeVoisine)
        elif(self.fonctionHeuristique == 2):
            return self.fonctionHeuristique2(noeud,villeVoisine)

        
    def fonctionCoutChoisi(self,noeud : Noeud,villeVoisine: Ville):
        """
            Cette fonction permet de choisir la fonction cout a utiliser

            Parametres
            ----------
            noeud : Noeud
                Le noeud courant
            villeVoisine : Ville
                La ville voisine du noeud courant
        """
        if(self.fonctionCout == 1):
            return self.fonctionCout1(noeud,villeVoisine)
        elif(self.fonctionCout == 2):
            return self.fonctionCout2(noeud,villeVoisine)
    
    def genererDescendants(self, noeud: Noeud):
        """
            Cette fonction permet de generer les descendants d'un noeud
            Les descendants sont les villes voisines de la ville courante à l'excpetion de la ville parent

            Parametres
            ----------
            noeud : Noeud
                Le noeud courant
        """
        descendants = []
        for voisin in noeud.ville.villesVoisines:
            if noeud.parent == None or voisin[0].nom != noeud.parent.ville.nom:
                descendants.append(Noeud(voisin[0],self.fonctionHeuristiqueChoisi(noeud,voisin),self.fonctionCoutChoisi(noeud,voisin),parent=noeud))
        return descendants    
    
    def noeudExisteDansListe(self, noeud: Noeud, liste: list):
        """
            Cette fonction permet de verifier si un noeud existe dans une liste

            Parametres
            ----------
            noeud : Noeud
                Le noeud courant
            liste : list
                La liste dans laquelle on cherche le noeud
            
            Retourne
            -------
                Noeud: le noeud si il existe dans la liste, None sinon
        """
        for noeudL in liste:
            if noeud == noeudL:
                return noeudL
        return None
    
    def genererCheminSolution(self,noeud: Noeud):
        """
            Cette fonction permet de generer le chemin solution a partir du noeud objectif

            Parametres
            ----------
            noeud : Noeud
                Le noeud solution
        """
        while noeud != None:
            noeud.setEtat("SOLUTION")
            print(noeud.ville.nom+" "+noeud.etat)
            noeud = noeud.parent

    def aEtoile(self):
        """
            Cette fonction implemente l'algorithme A*.

            Retourne
            -------
                Noeud: le noeud objectif si il est trouve, None sinon
        """
        ouvert = []
        ferme = []
        ouvert.append(self.initial)
        while ouvert:
            ouvert.sort(key=lambda x: x.f)
            noeudCourrant = ouvert.pop(0)
            ferme.append(noeudCourrant)
            noeudCourrant.setEtat("FERME")
            if noeudCourrant == self.objectif:
                self.genererCheminSolution(noeudCourrant)  
                return noeudCourrant
            enfants = self.genererDescendants(noeudCourrant)
            for enfant in enfants:
                enfant.setEtat("OUVERT")
                noeudExisteOuvert = self.noeudExisteDansListe(enfant, ouvert)
                noeudExisteFerme = self.noeudExisteDansListe(enfant, ferme)
                if noeudExisteOuvert == None and noeudExisteFerme == None:
                    ouvert.append(enfant)
                elif noeudExisteOuvert != None:
                    if noeudExisteOuvert.g > enfant.g:
                        ouvert.remove(noeudExisteOuvert)
                        ouvert.append(enfant)
                    else:
                        enfant.setEtat("ELIMINE")
                else:
                    if noeudExisteFerme.g > enfant.g:
                        ouvert.append(enfant)
                    else:
                        enfant.setEtat("ELIMINE")
        return None


initial = Noeud(smc)
objectif = Noeud(metz)

algo = aEtoile(initial, objectif)
n = algo.aEtoile()
# algo.genererCheminSolution(n)  

initial.afficherArbre()

initial.exporterArbre("test.dot")

# dot -Tpng test.dot -o test.png 