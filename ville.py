import math

class Ville: 
    """
        Classe représentant une ville

        Attributs:
        ----------
        nom : str
            Nom de la ville
        longitude : float
            Longitude de la ville
        latitude : float
            Latitude de la ville
        villesVoisines : list
            Liste des villes voisines de la ville(existe une route entre les deux villes)
    """

    def __init__(self, nom, longitude, latitude):
        self.nom = nom
        self.longitude = longitude
        self.latitude = latitude
        self.villesVoisines = []

    def distance(self, ville):
        """
            Calcule la distance entre la ville et une autre ville
            Selon la formule de Haversine
        """
        R = 6371 
        dLatitude = math.radians(ville.latitude - self.latitude)
        dLongitude = math.radians(ville.longitude - self.longitude)
        lat1 = math.radians(self.latitude)
        lat2 = math.radians(ville.latitude)
    
        a = math.sin(dLatitude / 2) * math.sin(dLatitude / 2) + \
            math.sin(dLongitude / 2) * math.sin(dLongitude / 2) * math.cos(lat1) * math.cos(lat2)
        c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
        distance = R * c
        return distance  

    def ajouterVoisin(self, voisin):
        """
            Ajoute une ville voisine à la ville courante et vice-versa 

            Paramètres:
            -----------
            voisin : Ville
                Ville voisine
        """
        distance = self.distance(voisin)
        self.villesVoisines.append((voisin, distance))
        voisin.villesVoisines.append((self, distance))