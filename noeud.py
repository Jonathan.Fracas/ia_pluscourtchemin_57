import operator
from pprint import pprint
import copy
from operator import itemgetter, attrgetter
import sys
from anytree import Node, RenderTree, AsciiStyle, PreOrderIter
from anytree.exporter import DotExporter
from anytree.dotexport import RenderTreeGraph
import math
from villes import *





class Noeud(Node):
    """
        Cette classe représente un noeud de l'arbre de recherche, le noeud represente l'etat du systeme.
        Herite de la classe Node de la librairie anytree.
        ...
        Attributs
        ----------
        ville : Ville
            Represente la ville du noeud
        h : int
            Represente la valeur de la fonction heuristique
        g : int
            Represente la valeur de la fonction cout 
        f : int
            Represente la valeur de la fonction f (somme de g et h)
        parent : Noeud
            Represente le parent du noeud (None si le noeud est la racine)
        etat : str
            Represente l'etat du noeud (OUVERT, FERME, ELIMINE, SOLUTION)
    
    """

    cptElimine =0

    def __init__(self, ville : Ville ,h =0,g=0,parent=None):
        self.ville = ville
        self.h = h
        self.g = g
        self.f = self.g + self.h
        super().__init__(self.name(), parent=parent)
        self.etat = None

    def name(self):
        """
            Cette fonction permet de retourner le nom du noeud
        """
        return f"{round(self.g,2)} {self.ville.nom} {round(self.h,2)}"
    
    def setName(self, h):
        """
            Cette fonction permet de mettre a jour le nom du noeud
        """
        self.h = h
        self.name = f"{round(self.g,2)} {self.ville.nom} {round(self.h,2)}"

    def __eq__(self, other):
        """
            Cette fonction permet de comparer deux noeuds
            Deux noeuds sont egaux si leur ville ont le meme nom 
        """
        if other == None:
            return False
        return self.ville.nom == other.ville.nom

    def setEtat(self,etat):
        """
            Cette fonction permet de mettre a jour l'etat du noeud
            Si le noeud est elimine on incremente le compteur de noeud elimine et on met a jour le nom du noeud.

            Parametres
            ----------
                etat: str:
                    L'etat du noeud
        """
        self.etat = etat
        if etat == "ELIMINE":
            Noeud.cptElimine += 1
            self.name = Noeud.cptElimine    
    
    def getColor(self):
        """
            Cette fonction permet de retourner la couleur du noeud en fonction de son etat (OUVERT, FERME, SOLUTION, ELIMINE)

            Retourne
            -------
                str: la couleur du noeud
        """
        if(self.etat == "OUVERT"):
            return "green"
        elif(self.etat == "FERME"):
            return "yellow"
        elif(self.etat == "SOLUTION"):
            return "red"
        else:
            return "black"

    def node_attributes(self):
        """
            Cette fonction permet de retourner la couleur du noeud pour l'affichage de l'arbre de recherche

            Retourne
            -------
                str: la chaine interprete par graphviz pour l'affichage de la couleur du noeud
        """
        return 'style="filled", fillcolor="{}"'.format(self.getColor())
    
    def exporterArbre(self, nomFichier):
        """
            Cette fonction permet d'exporter l'arbre de recherche dans un fichier .dot
            Elle doit etre appelee sur le noeud initial

            Parametres
            ----------
                nomFichier: str:
                    Le nom du fichier .dot
        """
        DotExporter(self, nodeattrfunc=lambda node: node.node_attributes()).to_dotfile(nomFichier)        
    

    def afficherArbre(self):
        """
            Cette fonction permet d'afficher l'arbre de recherche
            Elle doit etre appelee sur le noeud initial
        """
        for pre, fill, node in RenderTree(self):
            print("%s%s" % (pre, node.name))





