from ville import * 

villes = []

villes.append(Ville("aboncourt-sur-seille",19,3.63))
villes.append(Ville("roncourt",6.04223,49.2028))
villes.append(Ville("fey",6.1,49.0333))
villes.append(Ville("marly",6.14973,49.0612))
villes.append(Ville("hauconcourt",6.18333,49.2167))
villes.append(Ville("stiring-wendel",6.929,49.202))
villes.append(Ville("ennery",6.21667,49.2167))
villes.append(Ville("bassing",6.8,48.8667))
villes.append(Ville("argancy",6.2,49.2))
villes.append(Ville("montois-la-montagne",6.0167,49.2167))
villes.append(Ville("bambiderstroff",6.58333,49.1))
villes.append(Ville("morsbach",6.86667,49.1667))
villes.append(Ville("saint-privat-la-montagne",6.04,49.189))
villes.append(Ville("ars-laquenexy",6.25,6.28333))
villes.append(Ville("vitry-sur-orne",6.108,49.267))
villes.append(Ville("pierrevillers",6.1,49.2167))
villes.append(Ville("ban-saint-martin",4252,4275))
villes.append(Ville("amneville",6.15,49.2667))
villes.append(Ville("ay-sur-moselle",6.206,49.242))
villes.append(Ville("mondelange",6.16667,49.2667))
villes.append(Ville("uckange",6.15,49.3))
villes.append(Ville("jussy",6.08333,49.1))
villes.append(Ville("forbach",6.9,49.1833))
villes.append(Ville("saulny",6.1,49.1667))
villes.append(Ville("feves",6.11667,49.2))

villes.append(Ville("bronvaux",6.08333,49.2))
villes.append(Ville("rosselange",6.06667,49.2667))

villes.append(Ville("woippy",6.15,49.15))

villes.append(Ville("moulins-les-metz", 6.10659,49.106))

villes.append(Ville("maxe",6.19,49.167))

villes.append(Ville("freyming-merlebach",6.7833,49.15))


villes.append(Ville("rozerieulles",6.08333,49.1))

villes.append(Ville("amanvillers",6.03333,49.1667))

villes.append(Ville("metz",6.16667,49.1333))
villes.append(Ville("chailly-les-ennery",6.235,49.208))

villes.append(Ville("maizeroy",6.4,49.0833))

villes.append(Ville("noveant-sur-moselle",6.048,49.024 ))
villes.append(Ville("pouilly",6.18333,49.05))

villes.append(Ville("hombourg-haut",6.775558,49.125212))


villes.append(Ville("jouy-aux-arches",6.0833,49.0667))

villes.append(Ville("clouange",6.1,49.2667))

villes.append(Ville("hayange",6.05,49.3333))

villes.append(Ville("talange",6.18333,49.25))
villes.append(Ville("silly-sur-nied",6.361,49.118))

villes.append(Ville("marange-zondrange",6.5333,49.1167))

villes.append(Ville("petite-rosselle",6.857,49.209))

villes.append(Ville("ars-sur-moselle",6.0667,49.0833))

villes.append(Ville("raville",6.48333,49.0833))

villes.append(Ville("bousse",6.2,49.2833))

villes.append(Ville("ogy",6.31667,49.1))


villes.append(Ville("scy-chazelles",6.114,49.114))

villes.append(Ville("longeville-les-metz",6.135 ,49.115))

villes.append(Ville("coincy",6.28333,49.1))

villes.append(Ville("hagondange",6.16667,49.25))

villes.append(Ville("bettange",6.48333,49.2333))
villes.append(Ville("laquenexy",6.31667,49.0833))



villes.append(Ville("courcelles-chaussy",6.4053,49.102))
villes.append(Ville("remering",6.62862,49.2648))
villes.append(Ville("malroy",6.2,49.1833))

villes.append(Ville("augny",6.11667,49.0667))

villes.append(Ville("maizieres-les-metz",6.16052,49.20754))

villes.append(Ville("longeville-les-saint-avold",6.663208,49.107342))
villes.append(Ville("bionville-sur-nied",6.481,49.104))
villes.append(Ville("rosbruck",6.85,49.1667))


villes.append(Ville("rombas",6.08333,49.25))

villes.append(Ville("saint-avold",6.706,49.106))


villes.append(Ville("colligny",6.33333,49.1))

villes.append(Ville("verneville",6,49.15))



villes.append(Ville("semecourt",6.13333,49.1833))

villes.append(Ville("marange-silvange",6.1148,49.214295))


villes.append(Ville("tremery",6.25,49.25))

villes.append(Ville("sainte-marie-aux-chenes",6.004,49.2))

villes.append(Ville("ancy-sur-moselle",6.0667,49.05))

villes.append(Ville("montigny-les-metz",6.147,49.099068))
villes.append(Ville("lorry-les-metz",6.1217,49.1418))

villes.append(Ville("vaux",6.08028,49.0934))

villes.append(Ville("gravelotte",6.01667,49.1167))

villes.append(Ville("chatel-saint-germain",6.0833,49.1167))
villes.append(Ville("saint-julien-les-metz", 6.204 ,49.1333))
villes.append(Ville("vigy",6.3,49.2))

villes.append(Ville("malancourt-la-montagne",6.01901006699,49.241293))
villes.append(Ville("devant-les-ponts",6.1517084,49.1331646))
villes.append(Ville("magny",6.18798,49.076665))
villes.append(Ville("lessy",6.1,49.1167))
villes.append(Ville("knutange",6.03333,49.3333))
villes.append(Ville("angevillers",6.03333,49.3833))
villes.append(Ville("fontoy",6,49.35))
villes.append(Ville("lommerange",5.96667,49.3333))
villes.append(Ville("charly-oradour",6.25,49.1667))
villes.append(Ville("antilly",6.25,49.2))
villes.append(Ville("oeting",6.91667,49.1667))
villes.append(Ville("cocheren",6.85,49.15))
villes.append(Ville("guenviller",6.8,49.1))
villes.append(Ville("lachambre",6.75,49.0833))
villes.append(Ville("fouligny",6.5,49.1))
villes.append(Ville("narbefontaine",6.55,49.1333))
villes.append(Ville("haute-vigneulles",6.556,49.10))
villes.append(Ville("guinglange",6.51667,49.0667))
villes.append(Ville("marsilly",6.3,49.1))
villes.append(Ville("plappeville",6.11667,49.1333))
villes.append(Ville("norroy-le-veneur",6.107,49.179))
villes.append(Ville("moyeuvre-grande",6.0333,49.25))
villes.append(Ville("nilvange",6.05,49.35))
villes.append(Ville("chieulles",6.23333,49.1667))
villes.append(Ville("spicheren",6.96667,49.2))
villes.append(Ville("betting",6.81667,49.1167))
villes.append(Ville("valmont",6.7,49.0833))
villes.append(Ville("laudrefang",6.63333,49.0833))
villes.append(Ville("servigny-les-raville",6.447,49.077))


def getVille(nom):
    for ville in villes:
        if ville.nom == nom:
            return ville
    return None

smc = getVille("sainte-marie-aux-chenes")
smc.ajouterVoisin(getVille("saint-privat-la-montagne"))
smc.ajouterVoisin(getVille("montois-la-montagne"))
smc.ajouterVoisin(getVille("verneville"))

gravelotte = getVille("gravelotte")
gravelotte.ajouterVoisin(getVille("verneville"))
gravelotte.ajouterVoisin(getVille("ars-sur-moselle"))
gravelotte.ajouterVoisin(getVille("rozerieulles"))

rozerieulles = getVille("rozerieulles")
rozerieulles.ajouterVoisin(getVille("jussy"))
rozerieulles.ajouterVoisin(getVille("moulins-les-metz"))

ars = getVille("ars-sur-moselle")
ars.ajouterVoisin(getVille("ancy-sur-moselle"))
ars.ajouterVoisin(getVille("jouy-aux-arches"))
ars.ajouterVoisin(getVille("vaux"))

ancy = getVille("ancy-sur-moselle")
ancy.ajouterVoisin(getVille("noveant-sur-moselle"))

vaux = getVille("vaux")
vaux.ajouterVoisin(getVille("jussy"))
vaux.ajouterVoisin(getVille("montigny-les-metz"))

spm = getVille("saint-privat-la-montagne")
spm.ajouterVoisin(getVille("roncourt"))
spm.ajouterVoisin(getVille("marange-silvange"))
spm.ajouterVoisin(getVille("saulny"))
spm.ajouterVoisin(getVille("amanvillers"))

amanvillers = getVille("amanvillers")
amanvillers.ajouterVoisin(getVille("chatel-saint-germain"))
amanvillers.ajouterVoisin(getVille("lorry-les-metz"))

chatel = getVille("chatel-saint-germain")
chatel.ajouterVoisin(getVille("moulins-les-metz"))
chatel.ajouterVoisin(getVille("rozerieulles"))
chatel.ajouterVoisin(getVille("lessy"))

lessy = getVille("lessy")
lessy.ajouterVoisin(getVille("scy-chazelles"))
lessy.ajouterVoisin(getVille("plappeville"))


saulny = getVille("saulny")
saulny.ajouterVoisin(getVille("woippy"))
saulny.ajouterVoisin(getVille("lorry-les-metz"))
saulny.ajouterVoisin(getVille("devant-les-ponts"))
saulny.ajouterVoisin(getVille("norroy-le-veneur"))

lorry = getVille("lorry-les-metz")
lorry.ajouterVoisin(getVille("plappeville"))
lorry.ajouterVoisin(getVille("woippy"))

metz = getVille("metz")
metz.ajouterVoisin(getVille("devant-les-ponts"))
metz.ajouterVoisin(getVille("longeville-les-metz"))
metz.ajouterVoisin(getVille("montigny-les-metz"))
metz.ajouterVoisin(getVille("saint-julien-les-metz"))
metz.ajouterVoisin(getVille("laquenexy"))
metz.ajouterVoisin(getVille("magny"))
metz.ajouterVoisin(getVille("moulins-les-metz"))
metz.ajouterVoisin(getVille("ogy"))

magny = getVille("magny")
magny.ajouterVoisin(getVille("pouilly"))


roncourt = getVille("roncourt")
roncourt.ajouterVoisin(getVille("montois-la-montagne"))
roncourt.ajouterVoisin(getVille("pierrevillers"))
roncourt.ajouterVoisin(getVille("moyeuvre-grande"))
roncourt.ajouterVoisin(getVille("malancourt-la-montagne"))


malancourt = getVille("malancourt-la-montagne")
malancourt.ajouterVoisin(getVille("rombas"))

moyeuvre = getVille("moyeuvre-grande")
moyeuvre.ajouterVoisin(getVille("rosselange"))
moyeuvre.ajouterVoisin(getVille("hayange"))



rosselange = getVille("rosselange")
rosselange.ajouterVoisin(getVille("moyeuvre-grande"))
rosselange.ajouterVoisin(getVille("clouange"))

hayange = getVille("hayange")
hayange.ajouterVoisin(getVille("knutange"))
hayange.ajouterVoisin(getVille("nilvange"))

knutange = getVille("knutange")
knutange.ajouterVoisin(getVille("angevillers"))
knutange.ajouterVoisin(getVille("fontoy"))

angevillers = getVille("angevillers")
angevillers.ajouterVoisin(getVille("nilvange"))
angevillers.ajouterVoisin(getVille("lommerange"))
angevillers.ajouterVoisin(getVille("fontoy"))

rombas = getVille("rombas")
rombas.ajouterVoisin(getVille("amneville"))
rombas.ajouterVoisin(getVille("clouange"))
rombas.ajouterVoisin(getVille("vitry-sur-orne"))

marange = getVille("marange-silvange")
marange.ajouterVoisin(getVille("maizieres-les-metz"))
marange.ajouterVoisin(getVille("semecourt"))
marange.ajouterVoisin(getVille("feves"))

semecourt = getVille("semecourt")
semecourt.ajouterVoisin(getVille("talange"))
semecourt.ajouterVoisin(getVille("maxe"))
semecourt.ajouterVoisin(getVille("norroy-le-veneur"))
semecourt.ajouterVoisin(getVille("woippy"))
semecourt.ajouterVoisin(getVille("maizieres-les-metz"))

norroy = getVille("norroy-le-veneur")
norroy.ajouterVoisin(getVille("feves"))

talange = getVille("talange")
talange.ajouterVoisin(getVille("hagondange"))
talange.ajouterVoisin(getVille("ay-sur-moselle"))
talange.ajouterVoisin(getVille("maizieres-les-metz"))


amneville = getVille("amneville")
amneville.ajouterVoisin(getVille("hagondange"))
amneville.ajouterVoisin(getVille("mondelange"))

ay = getVille("ay-sur-moselle")
ay.ajouterVoisin(getVille("tremery"))
ay.ajouterVoisin(getVille("ennery"))
ay.ajouterVoisin(getVille("bousse"))
ay.ajouterVoisin(getVille("hauconcourt"))
ay.ajouterVoisin(getVille("argancy"))

argancy = getVille("argancy")
argancy.ajouterVoisin(getVille("malroy"))

malroy = getVille("malroy")
malroy.ajouterVoisin(getVille("saint-julien-les-metz"))
malroy.ajouterVoisin(getVille("chieulles"))
malroy.ajouterVoisin(getVille("charly-oradour"))
malroy.ajouterVoisin(getVille("antilly"))

antilly = getVille("antilly")
antilly.ajouterVoisin(getVille("chailly-les-ennery"))
antilly.ajouterVoisin(getVille("vigy"))

forbach = getVille("forbach")
forbach.ajouterVoisin(getVille("morsbach"))
forbach.ajouterVoisin(getVille("oeting"))
forbach.ajouterVoisin(getVille("spicheren"))
forbach.ajouterVoisin(getVille("stiring-wendel"))
forbach.ajouterVoisin(getVille("petite-rosselle"))

morsbach = getVille("morsbach")
morsbach.ajouterVoisin(getVille("rosbruck"))

rosbruck = getVille("rosbruck")
rosbruck.ajouterVoisin(getVille("freyming-merlebach"))
rosbruck.ajouterVoisin(getVille("cocheren"))


freyming = getVille("freyming-merlebach")
freyming.ajouterVoisin(getVille("hombourg-haut"))
freyming.ajouterVoisin(getVille("betting"))
freyming.ajouterVoisin(getVille("cocheren"))

hombourg = getVille("hombourg-haut")
hombourg.ajouterVoisin(getVille("saint-avold"))
hombourg.ajouterVoisin(getVille("guenviller"))

saintavold = getVille("saint-avold")
saintavold.ajouterVoisin(getVille("valmont"))
saintavold.ajouterVoisin(getVille("longeville-les-saint-avold"))
saintavold.ajouterVoisin(getVille("lachambre"))

longevillelessaintavold = getVille("longeville-les-saint-avold")
longevillelessaintavold.ajouterVoisin(getVille("bambiderstroff"))
longevillelessaintavold.ajouterVoisin(getVille("laudrefang"))
longevillelessaintavold.ajouterVoisin(getVille("marange-zondrange"))


marangezondrange = getVille("marange-zondrange")
marangezondrange.ajouterVoisin(getVille("fouligny"))
marangezondrange.ajouterVoisin(getVille("narbefontaine"))
marangezondrange.ajouterVoisin(getVille("haute-vigneulles"))

fouligny =getVille("fouligny")
fouligny.ajouterVoisin(getVille("raville"))
fouligny.ajouterVoisin(getVille("bionville-sur-nied"))
fouligny.ajouterVoisin(getVille("guinglange"))

raville = getVille("raville")
raville.ajouterVoisin(getVille("bionville-sur-nied"))
raville.ajouterVoisin(getVille("servigny-les-raville"))

courcelleschaussy = getVille("courcelles-chaussy")
courcelleschaussy.ajouterVoisin(getVille("bionville-sur-nied"))
courcelleschaussy.ajouterVoisin(getVille("servigny-les-raville"))
courcelleschaussy.ajouterVoisin(getVille("maizeroy"))
courcelleschaussy.ajouterVoisin(getVille("silly-sur-nied"))

sillysurnied = getVille("silly-sur-nied")
sillysurnied.ajouterVoisin(getVille("colligny"))
sillysurnied.ajouterVoisin(getVille("coincy"))
sillysurnied.ajouterVoisin(getVille("ogy"))


marsilly =getVille("marsilly")
marsilly.ajouterVoisin(getVille("colligny"))
marsilly.ajouterVoisin(getVille("coincy"))
marsilly.ajouterVoisin(getVille("laquenexy"))



